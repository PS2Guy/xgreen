## XGreen theme designed by [DigiXOne] (https://github.com/digixone/XGreen) ##

This theme is stored in <a href="https://github.com/PS2Guy/ZPanelX_Themes/tree/master/XGreen" target="_blank">this repository</a>, but this is the master branch.
